import React from 'react';

import './css/RoundedImage.css'

class RoundedImage extends React.Component{
    
    
    render(){
        return(
            <div>
                <img className='imgR' src={this.props.img} alt="IMG"/>
            </div>
        )
    }
}

export default RoundedImage;