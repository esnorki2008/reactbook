import React from "react";
import "./css/Side.css";

class Side extends React.Component {
 
  render() {
    return (
      <React.Fragment>
        <div className="sideComponent">
          <div className="container">
            {this.props.children}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Side;
