import React from "react";
import RoundedImage from "../RoundedImage";
import LeftMenu from "./LeftMenu";

import "./css/Left.css";
import profile from "../../images/profile.png";
import addf from "../../images/addf.png";

class Left extends React.Component {
  fRequest = 20;
  render() {
    return (
      <div>
        <div className="leftComponent container">
          <div className="row d-flex align-items-center containerLF">
            <div className="col-9">
              <h4>
                <b>Home</b>
              </h4>
            </div>
            <div className="col-2 afontLF">Create</div>
          </div>

          <div className="row d-flex align-items-center ">
            <div className="col-2">
              <div className="fullimgLF">
                <RoundedImage img={profile} />
              </div>
            </div>
            <div className="ifontLF">Josephine Williams</div>
          </div>

          <div className="row-2 d-flex align-items-center containerLFA">
            <div className="col-2 tabLF">
              <div className="fullimgLF">
                <img className="imgC" src={addf} alt="IMG" />
              </div>
            </div>
            <div className="tabTLF">{this.fRequest} pending friend request</div>
          </div>
          <hr className="solid"></hr>
       
          <LeftMenu/>
        </div>
       
        
      </div>
    );
  }
}

export default Left;
