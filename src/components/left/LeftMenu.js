import React from "react";
import "./css/LeftMenu.css";
import LeftMenuItem from "./LeftMenuItem";

import LM1 from "../../images/LM1.PNG";
import LM2 from "../../images/LM2.PNG";
import LM3 from "../../images/LM3.PNG";
import LM4 from "../../images/LM4.PNG";
import LM5 from "../../images/LM5.PNG";
import LM6 from "../../images/LM6.PNG";
import LM7 from "../../images/LM7.PNG";
import G1 from "../../images/G1.PNG";
class LeftMenu extends React.Component {
  options = [
    { item: 100, img: G1, txt: "Red Table Talk Group" },
    { item: 0, img: LM1, txt: "Events" },
    { item: 1, img: LM2, txt: "Saved" },
    { item: 2, img: LM3, txt: "pages" },
    { item: 3, img: LM4, txt: "Friends" },
    { item: 4, img: LM5, txt: "Messenger" },
    { item: 5, img: LM6, txt: "Games" },
    { item: 6, img: LM7, txt: "See More" },
  ];

  render() {
    return (
      <div className="">
        <ul className="list-unstyled">
          {this.options.map((item) => {
            return (
              <li key={item.item}>
                <LeftMenuItem item={item} />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default LeftMenu;
