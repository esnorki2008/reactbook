import React from "react";
class LeftMenuItem extends React.Component {
 
  render() {
    return (
      <div className="">
        <div className="row d-flex align-items-center ">
          <div className="col-2">
            <div className="fullimgLF ifontLF">
              <img className="imgC" src={this.props.item.img} alt="IMG" />
            </div>
          </div>
          <div className="ifontLF">{this.props.item.txt}</div>
        </div>
      </div>
    );
  }
}

export default LeftMenuItem;
