import React from "react";
import "./css/Activitie.css";
import profile from "../../images/profile.png";
import A1 from "../../images/A1.PNG";
import A2 from "../../images/A4.PNG";
import A3 from "../../images/A3.PNG";
import RoundedImage from "../RoundedImage";
class Activitie extends React.Component {
  render() {
    return (
      <div className="sepaac align-self-center">
        <div className="acdiv container ">
          <div className="row no-gutters  sepaac ">
            <div className="col-1 ">
              <div className="psA">
                <RoundedImage img={profile} />
              </div>
            </div>
            <div className="col-11">
              <input
                className="filterA"
                type="text"
                placeholder="What's on yout mind Josephine?"
              />
            </div>
          </div>
          <hr className="solid"></hr>
          <div className="row no-gutters    ">
            <div className="col-4 ptrA">
              <div className="d-flex justify-content-center ">
                <img className="imgA" src={A1} alt="IMG" />
                <p className='txta'>Photo/Video </p>
              </div>
            </div>
            <div className="col-4 ptrA">
              <div className="d-flex justify-content-center">
                <img className="imgA" src={A2} alt="IMG" />
                <p className='txta'>Tag Friends </p>
              </div>
            </div>
            <div className="col-4 ptrA">
              <div className="d-flex justify-content-center">
                <img className="imgA" src={A3} alt="IMG" />
                <p className='txta'>Feeling/Activity </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Activitie;
