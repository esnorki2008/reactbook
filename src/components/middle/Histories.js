import React from "react";
import "./css/Histories.css";
import H1 from "../../images/H1.jpeg";
import H2 from "../../images/H2.jpg";
import H3 from "../../images/H3.jpg";
import H4 from "../../images/H4.jpg";
import H5 from "../../images/H5.jpg";
import profile from "../../images/profile.png";
import profile2 from "../../images/profile2.jpg";
import profile3 from "../../images/profile3.jpg";
import profile4 from "../../images/profile4.jpg";
import profile5 from "../../images/profile5.jpg";

import History from "./History";
class Histories extends React.Component {
  histories = [
    { item: 0, img: H2, txt: "Add to Story",profile:profile },
    { item: 1, img: H3, txt: "Team Russo",profile:profile2 },
    { item: 2, img: H4, txt: "Maria Gu",profile:profile3 },
    { item: 3, img: H5, txt: "Diana White",profile:profile4 },
    { item: 100, img: H1, txt: "Walter Lopez",profile:profile5 },
  ];

  render() {
    return (
      <div className="row no-gutters d-flex justify-content-between">
        {this.histories.map((history) => {
          return (
             <History key={history.item} item={history} />
          );
        })}
        
      </div>
    );
  }
}

export default Histories;
