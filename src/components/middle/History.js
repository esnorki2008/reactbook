import React from 'react';
import "./css/History.css";
import RoundedImage from "../RoundedImage";
class History extends React.Component{
    
    
    render(){
        return(
            <div className='conH'>
                <img className='imgH' src={this.props.item.img} alt="IMG"/>
                <p className="topBT">{this.props.item.txt}</p>
                <div className="topRI"><RoundedImage img={this.props.item.profile} /></div>
            </div>
        )
    }
}

export default History;