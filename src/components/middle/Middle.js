import React from 'react';
import "./css/Middle.css";

import Histories from './Histories'
import Activitie from './Activitie'
import Posts from './Posts'

class Middle extends React.Component{
    
    
    render(){
        return(
            <div className="middleComponent">
                <Histories/>
                <Activitie/>
                <Posts/>
            </div>
        )
    }
}

export default Middle;