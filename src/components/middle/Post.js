import React from "react";
import "./css/Post.css";
import posti3 from "../../images/posti3.PNG";
import RoundedImage from "../RoundedImage";
import posti from "../../images/posti.PNG";
class Post extends React.Component {
  render() {
    return (
      <div className="postjmp">
        <div className="postdiv ">
          <div className="container">
            <div className="row no-gutters  sepaac ">
              <div className="col-1 ">
                <div className="psA">
                  <RoundedImage img={this.props.item.profile} />
                </div>
              </div>
              <div className="col-2">
                <p className="postName">Diana White</p>
                <div className="row ">
                  <div className="col-5">
                    <p className="postTimeName ">
                      6 hrs
                    </p>
                 
                  </div>
                  <div className="col-2 ">
                    <div className="postImgS">
                    <img className="imgPOSTi" src={posti} alt="IMG" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-9 d-flex justify-content-end">
                <div className="postImgS3">
                  
                  <RoundedImage img={posti3} />
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <p>{this.props.item.txt}</p>
          </div>
          <div>
            <img className="imgPOST" src={this.props.item.postc} alt="IMG" />
          </div>
          <br></br>
        </div>
      </div>
    );
  }
}

export default Post;
