import React from "react";
import "./css/Posts.css";
import Post from "./Post";
import profile from "../../images/profile4.jpg";
import postc from "../../images/postc.jpg";

class Posts extends React.Component {
  posts = [
    { item: 0, postc: postc, txt: "This has some great healty recipies", profile: profile },
  
  ];
  render() {
    return (
      <React.Fragment>
        {this.posts.map((post) => {
          return <Post key={post.item} item={post} />;
        })}
      </React.Fragment>
    );
  }
}

export default Posts;
