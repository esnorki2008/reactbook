import React from "react";
import "./css/Navbar.css";
import flogo from "../../images/flogo.png";
import messenger from "../../images/messenger.png";
import bell from "../../images/bell.png";
import profile from "../../images/profile.png";

import RoundedImage from "../RoundedImage";
import NavbarMenu from "./NavbarMenu";

//import { Link } from 'react-router-dom';
/*
<Link className="Navbar__brand" to="/">
                        <img className="Navbar__brand-logo" src={logo} alt="logo" />
                        <span className="font-weight-light">Platzi</span>
                        <span className="font-weight-bold">Conf</span>
                    </Link>
*/
class Navbar extends React.Component {
  render() {
    return (
      <div className="Navbar">
        <div className="row container-fluid">
          <div className="col-3">
            <div className="row ">
              <div className="col-2">
                <div className="fullimgNV">
                  <RoundedImage img={flogo} />
                </div>
              </div>
              <div className="col-9 input_container">
                <span>
                  <i className="fa fa-search icon"></i>
                </span>
                <span>
                  <input
                    className="filter"
                    type="text"
                    placeholder="Search Facebook"
                  />
                </span>
              </div>
            </div>
          </div>
          <div className="col-6">
            <NavbarMenu />
          </div>
          <div className="col-3 d-flex justify-content-end">
            <div className="row ">
              <div className="col-6"></div>
              <div className="col-2 imgSN">
                <div>
                  <div className="bgimgNV">
                    <RoundedImage img={messenger} />
                  </div>
                </div>
              </div>
              <div className="col-2 imgSN">
                <div className="bgimgNV">
                  <RoundedImage img={bell} />
                </div>
              </div>
              <div className="col-2 imgSN">
                <div className="fullimgNV">
                  <RoundedImage img={profile} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Navbar;
