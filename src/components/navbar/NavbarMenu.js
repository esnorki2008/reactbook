import React from "react";
import "./css/NavbarMenu.css";

class NavbarMenu extends React.Component {
  render() {
    return (
      <div className="row ">
        <div className="col-1">
        </div>
        <div className="col-2">
        <button className="btnNM btn ">
          <i className="fa fa-home iconNM"></i>
        </button>
        </div>
        <div className="col-2">
        <button className="btnNM btn">
          <i className="fa fa-video iconNM"></i>
        </button>
        </div>
        <div className="col-2">
        <button className="btnNM btn">
          <i className="fa fa-cart-plus iconNM"></i>
        </button>
        </div>
        <div className="col-2">
        <button className="btnNM btn">
          <i className="fa fa-users iconNM"></i>
        </button>
        </div>
        <div className="col-2">
        <button className="btnNM btn">
          <i className="fa fa-cubes iconNM"></i>
        </button>
        </div>
        <div className="col-1">
        </div>
      </div>
    );
  }
}

export default NavbarMenu;
