import React from "react";
import "./css/Add.css";
import RoundedImage from "../RoundedImage";
import add1 from "../../images/add1.jpg";
import addc from "../../images/addc.jpg";
class Add extends React.Component {
  adds = [
    {
      item: 1,
      img: add1,
      imgContent: addc,
      name: "Lebo's Pizza",
      txt:
        "Experience the trendy pizza spot in Palo Alto being called the next big thing.",
    },
  ];

  render() {
    return (
      <div>
        <p className="thRH">Sponsored</p>
        {this.adds.map((add) => {
          return <AddItem key={add.item} item={add} />;
        })}
        <hr className="solid"></hr>
      </div>
    );
  }
}

class AddItem extends React.Component {
  render() {
    return (
      <div>
        
        <div className="row ">
          <div className="col-2">
            <div className="imgsize">
              <RoundedImage img={this.props.item.img} />
            </div>
          </div>
          <div className="col-10">
            <p className="nameadd">{this.props.item.name}</p>

            <p className="txtadd">{this.props.item.txt}</p>
            <img className="imgADD" src={this.props.item.imgContent} alt="IMG" />
          </div>
        </div>
        
      </div>
    );
  }
}
export default Add;
