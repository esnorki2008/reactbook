import React from "react";
import "./css/Birthday.css";
import gift from "../../images/gift.png";
class Birthday extends React.Component {
  births = [
    {
      item: 1,
      txt: "Jessica and 2 others have birthdays today.",
    },
  ];

  render() {
    return (
      <div>
        <p className="thRH">Birthdays</p>
        {this.births.map((birth) => {
          return <BirthdayItem key={birth.item} item={birth} />;
        })}

        <hr className="solid"></hr>
      </div>
    );
  }
}

class BirthdayItem extends React.Component {
  render() {
    return (
      <div>
        <div className="row ">
          <div className="col-2">
            <div className="imgsize">
              <img className="imgBirth" src={gift} alt="IMG" />
            </div>
          </div>
          <div className="col-10">
           

            <p className="txtBirth">{this.props.item.txt}</p>
           
          </div>
        </div>
      </div>
    );
  }
}

export default Birthday;
