import React from "react";
import "./css/Contact.css";
import profile2 from "../../images/profile2.jpg";
import profile3 from "../../images/profile3.jpg";
import RoundedImage from "../RoundedImage";
class Contact extends React.Component {
  contacts = [
    { item:1,img: profile2, name: "Dennis Han" },
    { item:2,img: profile3, name: "Erica Jones" },
  ];

  render() {
    return (
      <div>
        <p className="thRH">Contacts</p>
        {this.contacts.map((contact) => {
          return <ContactItem key={contact.item} item={contact} />;
        })}

        <hr className="solid"></hr>
      </div>
    );
  }
}

class ContactItem extends React.Component {
  render() {
    return <div>
        <div className="row ">
          <div className="col-2">
            <div className="imgsize">
              <RoundedImage img={this.props.item.img} />
            </div>
          </div>
          <div className="col-10">
           

            <p className="txtCont">{this.props.item.name}</p>
           
          </div>
        </div>
    </div>;
  }
}

export default Contact;
