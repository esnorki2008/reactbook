import React from 'react';
import './css/Right.css'

import Contact from './Contact'
import Add from './Add'
import Birthday from './Birthday'
class Right extends React.Component{
    
    
    render(){
        return(
            <div className='containerRH'>
                <Add/>
                <Birthday/>
                <Contact/>
            </div>
        )
    }
}

export default Right;