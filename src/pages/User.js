import React from "react";
import Left from "../components/left/Left";
import Right from "../components/right/Right";
import Navbar from "../components/navbar/Navbar";
import "./css/User.css";
import Middle from "../components/middle/Middle";
class User extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="Home">
          <div className="row ">
            <div className="col-3 ">
        
              <Left className />
            </div>
            <div className="col-6">
              <Middle />
            </div>
            <div className="col-3">
              <Right />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default User;
